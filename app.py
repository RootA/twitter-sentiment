import re 
import tweepy 
from tweepy import OAuthHandler 
from textblob import TextBlob 
from flask import Flask, jsonify, request
from twitter import TwitterClient

import cherrypy

app = Flask(__name__)


@app.route('/twitter/query', methods=['POST'])
def TwitterAPIQuery():
	keyword = request.json['query']

	if not keyword:
		return jsonify({'message' : 'Enter a query parameter E.G. Uhuru Kenyatta'}), 422
	
	return passQuery(keyword)
	

def passQuery(keyword): 
	# creating object of TwitterClient Class 
	api = TwitterClient() 
	# calling function to get tweets 
	tweets = api.get_tweets(query = keyword, count = 200)

	if not tweets: 
		return jsonify({'message' : 'No Tweets found'}), 412

	# picking positive tweets from tweets 
	ptweets = [tweet for tweet in tweets if tweet['sentiment'] == 'positive'] 

	# picking negative tweets from tweets 
	ntweets = [tweet for tweet in tweets if tweet['sentiment'] == 'negative'] 

	postive_sample_set = []
	for tweet in ptweets[:100]:
		response = {}
		response['tweet'] = tweet['text']
		postive_sample_set.append(response)
	
	negative_smaple_set = []
	for tweet in ntweets[:100]:
		response = {}
		response['tweet'] = tweet['text']
		negative_smaple_set.append(response)

	responseObject = {
		'tweets_analyzed' : len(tweets),
		'positive_tweets' : 100*len(ptweets)/len(tweets),
		'negative_tweets' : 100*len(ntweets)/len(tweets),
		'neutral_tweets' : 100*(len(tweets)-len(ntweets)-len(ptweets))/len(tweets),
		'top_positive_tweets' : postive_sample_set,
		'top_negative_tweets': negative_smaple_set 
	}

	return jsonify(responseObject), 200
	# print("Number of Tweets {}".format(len(tweets)))
	
	# percentage of positive tweets 
	# print("Positive tweets percentage: {} %".format(100*len(ptweets)/len(tweets))) 
	
	# percentage of negative tweets 
	# print("Negative tweets percentage: {} %".format(100*len(ntweets)/len(tweets))) 
	# percentage of neutral tweets 
	# print("Neutral tweets percentage: {} %".format(100*(len(tweets)-len(ntweets)-len(ptweets))/len(tweets))) 
  
	# printing first 5 positive tweets 
	# print("\n\nPositive tweets:") 
	# for tweet in ptweets[:10]: 
	# 	print(tweet['text']) 
  
	# # printing first 5 negative tweets 
	# print("\n\nNegative tweets:") 
	# for tweet in ntweets[:10]: 
	# 	print(tweet['text']) 
  
if __name__ == "__main__": 
	cherrypy.tree.graft(app, "/")
	# Unsubscribe the default server
	cherrypy.server.unsubscribe()

	# Instantiate a new server object
	server = cherrypy._cpserver.Server()

	# Configure the server object
	server.socket_host = "0.0.0.0"
	server.socket_port = 5000
	server.thread_pool = 30
	server.log = True
	server.screen = True

	# For SSL Support
	# server.ssl_module            = 'pyopenssl'
	# server.ssl_certificate       = 'ssl/certificate.crt'
	# server.ssl_private_key       = 'ssl/private.key'
	# server.ssl_certificate_chain = 'ssl/bundle.crt'

	# Subscribe this server
	server.subscribe()

	# Start the server engine (Option 1 *and* 2)
	cherrypy.engine.start()
	cherrypy.engine.block()